﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EsToWebGrid.Startup))]
namespace EsToWebGrid
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

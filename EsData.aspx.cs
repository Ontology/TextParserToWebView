﻿using ElasticSearchNestConnector;
using EsToWebGrid.Models;
using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EsToWebGrid
{
    public partial class EsData : System.Web.UI.Page
    {
        private OntologyModDBConnector dbConnector;
        private string idParser;
        private string esIndex;
        private string esType;
        private string esServer;
        private int esPort;
        private List<FieldItem> fieldItems;

        private clsLogStates logStates = new clsLogStates();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            idParser = Request.Params["IdParser"];
            GetIndexAndType();
            var header = GetHeader();
            Response.ContentType = "text/json";

            var dbConnector = new clsUserAppDBSelector(esServer, esPort, esIndex.ToLower(), 10000, Guid.NewGuid().ToString().Replace("-", ""));
            var documents = dbConnector.GetData_Documents(strType: esType, query:"*");
            int id = 0;
            using (var textWriter = new System.IO.StreamWriter(Response.OutputStream))
            {

                var first = true;
                //textWriter.WriteLine("{" + header + ", ");
                textWriter.WriteLine("data={");
                textWriter.WriteLine("rows:[");
                //var rowData = "<rows>\r\n";
                documents.ForEach(doc =>
                {
                    if (first)
                    {
                        
                        first = false;
                    }
                    else
                    {
                        textWriter.Write(", ");
                    }
                        textWriter.WriteLine(JsonRow.GetJsonRow(doc, id, fieldItems));
                        
                    //rowData += "<row id = \"" + doc.Id + "\">\r\n";
                    //foreach (string key in doc.Dict.Keys)
                    //{
                    //    rowData += "<cell><![CDATA[" + doc.Dict[key].ToString()
                    //        + "]]></cell>\r\n";
                    //}
                    //rowData += "</row>";
                    id++;
                });
                textWriter.WriteLine("] }");
                //rowData += "</rows>\r\n";
            }

        }

        private string GetHeader()
        {
            fieldItems = new List<FieldItem>();
            var searchFieldParser = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idParser,
                    ID_RelationType = "d6ba58f352904913afa0a5c3bacccaf6",
                    ID_Parent_Other = "0b5d3ad33877418ca23c2779fbdf2d0a"
                }
            };

            var result = dbConnector.GetDataObjectRel(searchFieldParser);

            var idFieldParser = "";
            if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
            {
                idFieldParser = dbConnector.ObjectRels.First().ID_Other;

            }

            if (!string.IsNullOrEmpty(idFieldParser))
            {
                var searchFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idFieldParser,
                        ID_RelationType = "721ae9855d3e4f85b05b0bc0c6f565d2",
                        ID_Parent_Other = "bd8af19406a74130acad0394cab33ee6"
                    }
                };

                result = dbConnector.GetDataObjectRel(searchFields);

                if (result.GUID == logStates.LogState_Success.GUID)
                {
                    fieldItems = dbConnector.ObjectRels.OrderBy(field => field.OrderID).ThenBy(field => field.Name_Other).Select(field => new FieldItem
                    {
                        FieldId = field.ID_Other,
                        FieldName = field.Name_Other
                    }).ToList();
                }

                if (fieldItems.Any())
                {
                    var searchDataTypes = fieldItems.Select(field => new clsObjectRel
                    {
                        ID_Object = field.FieldId,
                        ID_RelationType = "c536d70360484832a02bba82fdfbbdc6",
                        ID_Parent_Other = "69e1c4ac593e4ebb907c9b2bcec595de"
                    }).ToList();

                    result = dbConnector.GetDataObjectRel(searchDataTypes);

                    if (result.GUID == logStates.LogState_Success.GUID)
                    {
                        fieldItems = (from resultField in fieldItems
                                      join dataType in dbConnector.ObjectRels on resultField.FieldId equals dataType.ID_Object
                                        select new FieldItem
                                        {
                                            FieldId = resultField.FieldId,
                                            FieldName = resultField.FieldName,
                                            FieldType = dataType.Name_Other
                                        }).ToList();
                    }
                    else
                    {
                        fieldItems.Clear();
                    }
                }
            }
            var header = "";

            header = "head:[\r\n";
            var first = true;
            fieldItems.ForEach(field =>
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    header += ", ";
                }
                header += "\t{";
                header += "id:\"" + (field.FieldName.ToLower() == "id" ? "iditem" : field.FieldName.ToLower()) + "\", ";
                header += "width:50, ";
                header += "type:\"ro\", ";
                header += "align:\"left\", ";
                header += "sort:\"" + field.DHTMLXSort + "\", ";
                header += "value:\"" + field.FieldName + "\"}\r\n";
            });

            header += "]";

            return header;
        }
        private void GetIndexAndType()
        {
            dbConnector = new OntologyModDBConnector("localhost", 9200, "ontology_db", "reports", 10000, Guid.NewGuid().ToString().Replace("-", ""));

            var searchIndex = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idParser,
                    ID_RelationType = "92a087edcd2e4f5e93b8c42f295be063",
                    ID_Parent_Other = "1233afbe60bf40e7922864ca4013b75c"
                }
            };

            var result = dbConnector.GetDataObjectRel(searchIndex);
            if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
            {
                var idIndex = dbConnector.ObjectRels.First().ID_Other;
                esIndex = dbConnector.ObjectRels.First().Name_Other;

                var searchServerPort = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idIndex,
                        ID_RelationType = "e07469d9766c443e85266d9c684f944f",
                        ID_Parent_Other = "9ba24283a0ad4717be306f0f3818c36c"
                    }
                };

                result = dbConnector.GetDataObjectRel(searchServerPort);

                
            }
            if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
            {
                var idServerPort = dbConnector.ObjectRels.First().ID_Other;

                var searchServer = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = idServerPort,
                            ID_RelationType = "d34d545e9ddf46cebb6f22db1b7bb025",
                            ID_Parent_Other = "d7a03a35875142b48e0519fc7a77ee91"
                        }
                    };

                result = dbConnector.GetDataObjectRel(searchServer);
                if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
                {
                    esServer = dbConnector.ObjectRels.First().Name_Other;
                    var searchPort = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = idServerPort,
                            ID_RelationType = "d34d545e9ddf46cebb6f22db1b7bb025",
                            ID_Parent_Other = "ca4eff30a40b476d99069f56a67c8cf9"
                        }
                    };

                    result = dbConnector.GetDataObjectRel(searchPort);
                }
                

            }

            if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
            {
                var sPort = dbConnector.ObjectRels.First().Name_Other;

                int.TryParse(sPort, out esPort);
                
            }

            if (result.GUID == logStates.LogState_Success.GUID)
            {
                var searchType = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = idParser,
                            ID_RelationType = "796712399c8f493cb5e749700f9543f4",
                            ID_Parent_Other = "d9775bda1526475b8e16723881828876"
                        }
                    };

                result = dbConnector.GetDataObjectRel(searchType);

                if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
                {
                    esType = dbConnector.ObjectRels.First().Name_Other;
                }
            }
        }
    }
}
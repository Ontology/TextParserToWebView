﻿using ElasticSearchNestConnector;
using EsToWebGrid.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EsToWebGrid.Controllers
{
    public class ElasticDataController : Controller
    {
        private clsLogStates logStates = new clsLogStates();

        public ActionResult ElasticData(string ID)
        {
            ViewBag.IdParser = ID;
            SetGridConfiguration();
            return View();
        }

            private void SetGridConfiguration()
        {
            var dbConnector = new OntologyModDBConnector("localhost", 9200, "ontology_db", "reports", 10000, Guid.NewGuid().ToString().Replace("-", ""));

            var resultFields = new List<FieldItem>();
            var searchFieldParser = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = ViewBag.IdParser,
                    ID_RelationType = "d6ba58f352904913afa0a5c3bacccaf6",
                    ID_Parent_Other = "0b5d3ad33877418ca23c2779fbdf2d0a"
                }
            };

            var result = dbConnector.GetDataObjectRel(searchFieldParser);

            var idFieldParser = "";
            if (result.GUID == logStates.LogState_Success.GUID && dbConnector.ObjectRels.Any())
            {
                idFieldParser = dbConnector.ObjectRels.First().ID_Other;

            }

            if (!string.IsNullOrEmpty(idFieldParser))
            {
                var searchFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = idFieldParser,
                        ID_RelationType = "721ae9855d3e4f85b05b0bc0c6f565d2",
                        ID_Parent_Other = "bd8af19406a74130acad0394cab33ee6"
                    }
                };

                result = dbConnector.GetDataObjectRel(searchFields);

                if (result.GUID == logStates.LogState_Success.GUID)
                {
                    resultFields = dbConnector.ObjectRels.OrderBy(field => field.OrderID).ThenBy(field => field.Name_Other).Select(field => new FieldItem
                    {
                        FieldId = field.ID_Other,
                        FieldName = field.Name_Other
                    }).ToList();
                }

                if (resultFields.Any())
                {
                    var searchDataTypes = resultFields.Select(field => new clsObjectRel
                    {
                        ID_Object = field.FieldId,
                        ID_RelationType = "c536d70360484832a02bba82fdfbbdc6",
                        ID_Parent_Other = "69e1c4ac593e4ebb907c9b2bcec595de"
                    }).ToList();

                    result = dbConnector.GetDataObjectRel(searchDataTypes);

                    if (result.GUID == logStates.LogState_Success.GUID)
                    {
                        resultFields = (from resultField in resultFields
                                        join dataType in dbConnector.ObjectRels on resultField.FieldId equals dataType.ID_Object
                                        select new FieldItem
                                        {
                                            FieldId = resultField.FieldId,
                                            FieldName = resultField.FieldName,
                                            FieldType = dataType.Name_Other
                                        }).ToList();
                    }
                    else
                    {
                        resultFields.Clear();
                    }
                }
            }
            var header = "";
            var colTypes = "";
            var sortTypes = "";
            var filterConfig = "";

            var first = true;
            resultFields.ForEach(field =>
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    header += ",";
                    colTypes += ",";
                    sortTypes += ",";
                    filterConfig += ",";
                }
                header += field.FieldName;
                colTypes += "ro";
                sortTypes += field.DHTMLXSort;
                filterConfig += "#text_filter";
            });


            ViewBag.ColumnNames = header;
            ViewBag.SortTypes = sortTypes;
            ViewBag.ColTypes = colTypes;
            ViewBag.FilterConfig = filterConfig;
        }
 

    }

}
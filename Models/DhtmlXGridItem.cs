﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EsToWebGrid.Models
{
    public class DhtmlXGridItem
    {
        public string Name { get; set; }
        public string Caption { get; set; }
        public string Type { get; set; }
    }
}
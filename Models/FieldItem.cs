﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EsToWebGrid.Models
{
    public class FieldItem
    {
        public string FieldId { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        
        public string DHTMLXSort
        {
            get
            {
                if (FieldType == "double")
                {
                    return SortingTypes.Integer;
                }
                else if (FieldType == "string")
                {
                    return SortingTypes.String;
                }
                else if (FieldType == "datetime")
                {
                    return SortingTypes.Datetime;
                }
                else if (FieldType == "int")
                {
                    return SortingTypes.Integer;
                }
                else
                {
                    return SortingTypes.Unsorted;
                }
            }
        }
    }
}
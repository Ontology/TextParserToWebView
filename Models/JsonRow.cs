﻿using Newtonsoft.Json;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EsToWebGrid.Models
{
    public static class JsonRow
    {
        
        public static string GetJsonRow(clsAppDocuments doc, int ix, List<FieldItem> fields)
        {
            var json = "{ id:\"" + doc.Id + "\", \r\ndata:[ ";
            //var json = "{ id:" + ix.ToString() + ",\r\n";
            var first = true;

            var keys = from field in fields
                       join key in doc.Dict.Keys on field.FieldName equals key
                       select field.FieldName;

            foreach (var key in keys)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    json += ", ";
                }

                //json += (key.ToLower() == "id" ? "iditem" : key.ToLower()) + ":" + "\"" + doc.Dict[key].ToString() + "\"\r\n";
                
                json += "\"" + doc.Dict[key].ToString() + "\"" + "\r\n";
            }
            json += "] }\r\n";
            return json;
        }
    }
}